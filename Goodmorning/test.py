import main


def test_say(string):
    assert main.say(string) == string


if __name__ == "__main__":
    test_say("Good morning Ms Hoa")
    test_say("Good morning Lan")
    test_say("Good morning Vietnam")
    print("Everything passed")
